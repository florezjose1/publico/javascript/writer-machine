function writer(element, text, interval) {
    let el = document.getElementById(element);
    if(el) {
        let long = text.length;
        let i = 0;

        let timer = setInterval(function () {
            // agregamos caracter por caracter al elemento desde text en la posición {i} y al final "_" para
            // simular la máquina de escribir
            el.innerHTML = el.innerHTML.substr(0, el.innerHTML.length - 1) + text.charAt(i) + "_";
            // si hemos llegado al final del string, entonces:
            if (i >= long) {
                // nos salimos del timer y eliminamos la barra "_"
                clearInterval(timer);
                el.innerHTML = el.innerHTML.substr(0, long);
                return true;
            } else {
                // de lo contrario... solo incrementamos {i}
                i ++;
            }
        }, interval)
    }
}


(function () {
    let el = "app";
    let text = "Welcome to example of writer machine";
    let interval = 100; // mili-segundos
    writer(el, text, interval);
})();